#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <SFML/Graphics.hpp>

using std::string;
using std::vector;

const vector <std::pair <sf::Keyboard::Key, string>> keys{
	{sf::Keyboard::W,     "W"},
	{sf::Keyboard::S,     "S"},
	{sf::Keyboard::A,     "A"},
	{sf::Keyboard::D,     "D"},
	{sf::Keyboard::Q,     "Crouch"},
	{sf::Keyboard::Space, "Space"},
	{sf::Keyboard::Num1,  "1"},
	{sf::Keyboard::Num2,  "2"},
	{sf::Keyboard::Num3,  "3"},
	{sf::Keyboard::R,     "Restart"},
	{sf::Keyboard::P,     "Kill"}
};

const unsigned width  = 300;
const unsigned height = 1000;
const unsigned charSize = 40;
const unsigned defaultVerticalTextSpace = 10;
const unsigned defaultHorizontalTextSpace = 4*defaultVerticalTextSpace;

class SFMLExtension{
	bool holdingM1 = false;
	bool holdingM2 = false;

	public:

	bool m1_pressed(){
		const bool pressed = sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
		const bool out = (!holdingM1) && pressed;
		holdingM1 = pressed;
		return out;
	}

	bool m2_pressed(){
		const bool pressed = sf::Mouse::isButtonPressed(sf::Mouse::Button::Right);
		const bool out = (!holdingM2) && pressed;
		holdingM2 = pressed;
		return out;
	}

	bool text_clicked(const sf::RenderWindow &window, const sf::Text text){
		const sf::Vector2i mousePos = sf::Mouse::getPosition(window);
		return text.getGlobalBounds().contains(mousePos.x, mousePos.y) && m1_pressed();
	}

	sf::Text to_sftext(const string str, sf::Font &font, const unsigned charSize, const sf::Color color={255,255,255}, const unsigned x=0, const unsigned y=0){
		sf::Text text;
		text.setFont(font);
		text.setString(str);
		text.setCharacterSize(charSize);
		text.setFillColor(color);
		text.setPosition(x,y);
		return text;
	}
};

template <typename T>
string to_str(const T val){
	std::ostringstream convert;
	convert << val;
	return convert.str();
}

bool adblock(){
	return system("pkill steamwebhelper")==0;
}

int main(){
	SFMLExtension extension;

	sf::Font monospace;
	monospace.loadFromFile("monospace.otf");

	unsigned textX=0;
	unsigned textY=0;
	vector <sf::Text> texts{};
	for (auto key : keys){
		sf::Text text = extension.to_sftext(key.second, monospace, charSize, {255,255,255}, textX, textY);

		texts.push_back(text);
		textY += text.getLocalBounds().height + defaultVerticalTextSpace;
	}

	sf::Text m1Text = extension.to_sftext("m1", monospace, charSize, {255,255,255}, textX, textY);
	textY += m1Text.getLocalBounds().height + defaultVerticalTextSpace;
	sf::Text m2Text = extension.to_sftext("m2", monospace, charSize, {255,255,255}, textX, textY);
	textY += m2Text.getLocalBounds().height + defaultVerticalTextSpace;
	texts.push_back(m1Text);
	texts.push_back(m2Text);

	sf::Text adblockText = extension.to_sftext("Adblock:off", monospace, charSize, {255,255,255}, textX, textY);
	textY += m2Text.getLocalBounds().height + defaultVerticalTextSpace;
	sf::Text adblockedText = extension.to_sftext("Ad blocked", monospace, charSize, {255,255,255}, textX, textY);
	textY += m2Text.getLocalBounds().height + defaultVerticalTextSpace;

	sf::Text clockText = extension.to_sftext("", monospace, charSize, {0,255,255}, textX, textY);

	sf::RenderWindow window(sf::VideoMode(width, height), "TF2_Spdrun");
//	window.setFramerateLimit(60);

	bool adblockOn = false;
	bool adblockedFading = false;
	unsigned char adblockedFade = 255;

	sf::Clock clock;
	while (window.isOpen()){
		sf::Event e;
		while (window.pollEvent(e)){
			if (e.type == sf::Event::Closed)
				window.close();
		}

		window.clear();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			clock.restart();

		for (unsigned i=0; i < keys.size(); i++){
			auto key = keys[i];
			if (sf::Keyboard::isKeyPressed(key.first))
				window.draw(texts[i]);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			window.draw(texts[texts.size()-2]);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
			window.draw(texts[texts.size()-1]);

		window.draw(adblockText);
		if (extension.text_clicked(window, adblockText)){
			adblockOn = !adblockOn;
			if (adblockOn) adblockText.setString("Adblock:on");
			else adblockText.setString("Adblock:off");
		}

		if (adblockOn){
			if (adblock())
				adblockedFading = true;
		}

		if (adblockedFading){
			window.draw(adblockedText);
			adblockedText.setFillColor({adblockedFade,adblockedFade,adblockedFade});
			--adblockedFade;
			if (adblockedFade == 255)
				adblockedFading = false;
		}

		clockText.setString(to_str(clock.getElapsedTime().asMilliseconds()/10));
		window.draw(clockText);
		window.display();
	}
}
